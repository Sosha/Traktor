#!/bin/bash

##########################################
#               Traktor V2               #
# https://gitlab.com/TraktorPlus/Traktor #
##########################################


clear
echo -e "Traktor V2 Beta\nTor will be automatically installed and configured…\n\n"


# =========/Create File and Folder/=========

if [ ! -e "$HOME/.Traktor/Traktor_Log/install.log" ]; then
    echo -e "Traktor Log: [$(date)]" | tee $HOME/.Traktor/Traktor_Log/install.log > /dev/null # echo date and time
fi

# =========/Create File and Folder/=========


# =========/Color/=========

GRE='\033[0;32m' # Green Light
NC='\033[0m' # White
RD='\033[91m' # Red Light

# =========\Color\=========


# =========\Functions\=========

function loading {
	spin[0]="-"
	spin[1]="\\"
	spin[2]="|"
	spin[3]="/"
	PID=$!
	echo -n "${spin[0]}"
	while [ -d /proc/$PID ]
	do
        for i in "${spin[@]}"
            do
                echo -ne "\b$i"
                sleep 0.2
        done
	done
}

function dne {
	echo -e "\b${GRE}Done."
}

function filed {
    echo -e "\b${RD}Failed!"
}

function packin {
	sudo apt-get update
	echo ""
	sudo apt-get install -y \
        tor \
        obfs4proxy \
		dnscrypt-proxy \
        privoxy \
		torbrowser-launcher \
		apt-transport-tor
	echo ""
}

function torrcbackup {
	if [ -e "/etc/tor/torrc" ]; then
		sudo cp /etc/tor/torrc /etc/tor/torrc.traktor-backup
	fi
}

function bridgewrite {
	sudo wget https://gitlab.com/TraktorPlus/Traktor/raw/config/torrc -O /etc/tor/torrc
	echo ""
}

function fixappar { 
	linecheck=`cat /etc/apparmor.d/abstractions/tor | head -19 | tail -1 | awk {'print $2$3$4'}`
    if [ "$linecheck" != "Neededbyobfs4proxy" ];then
        sudo sed -i '27s/PUx/ix/' /etc/apparmor.d/abstractions/tor
		sudo apparmor_parser -r -v /etc/apparmor.d/system_tor
    fi
}

function privoxyback {
	if [ -e "/etc/privoxy/config" ]; then
	        sudo cp /etc/privoxy/config /etc/privoxy/config.traktor-backup
	fi
}

function privoxyconf {
	sudo perl -i -pe 's/^listen-address/#$&/' /etc/privoxy/config
	echo 'logdir /var/log/privoxy
listen-address  0.0.0.0:8118
forward-socks5t             /     127.0.0.1:9050 .
forward         192.168.*.*/     .
forward            10.*.*.*/     .
forward           127.*.*.*/     .
forward           localhost/     .' | sudo tee /etc/privoxy/config
    echo ""
	sudo systemctl enable privoxy
	sudo systemctl restart privoxy.service
	echo ""
}

function stopnet {
	sudo sed -i -e 's/\[main\]/\[main\]\ndns=none/g' /etc/NetworkManager/NetworkManager.conf
}

function setip {
	gsettings set org.gnome.system.proxy mode 'manual'
	gsettings set org.gnome.system.proxy.http host 127.0.0.1
	gsettings set org.gnome.system.proxy.http port 8118
	gsettings set org.gnome.system.proxy.socks host 127.0.0.1
	gsettings set org.gnome.system.proxy.socks port 9050
	gsettings set org.gnome.system.proxy ignore-hosts "['localhost', '127.0.0.0/8', '::1', '192.168.0.0/16', '192.168.8.1', '10.0.0.0/8', '172.16.0.0/12', '0.0.0.0/8', '10.0.0.0/8', '100.64.0.0/10', '127.0.0.0/8', '169.254.0.0/16', '172.16.0.0/12', '192.0.0.0/24', '192.0.2.0/24', '192.168.0.0/16', '192.88.99.0/24', '198.18.0.0/15', '198.51.100.0/24', '203.0.113.0/24', '224.0.0.0/3']"
}

#function chkowner {
#    sudo chown debian-tor:adm /var/log/tor/log
#}

function boot {
    echo -e "Tor is trying to establish a connection. This may take long for some minutes. Please wait" | sudo tee /var/log/tor/log
    bootstraped='n'
    sudo service tor restart
    while [ $bootstraped == 'n' ]; do
	    if sudo cat /var/log/tor/log | grep "Bootstrapped 100%: Done"; then
		    bootstraped='y'
	    else
		    sleep 1
 	    fi 
    done
   echo ""
}

function addrepo {
    codename=`lsb_release -c | awk {'print $2'}`
    if [[ "$codename" =~ ^(trusty|xenial|zesty|artful|bionic|wheezy|jessie|stretch|buster|sid|experimental)$ ]]; then
        echo "deb tor+http://deb.torproject.org/torproject.org $codename main" | sudo tee /etc/apt/sources.list.d/tor.list
	    echo "deb-src tor+http://deb.torproject.org/torproject.org $codename main" | sudo tee -a /etc/apt/sources.list.d/tor.list
	    echo "deb tor+http://deb.torproject.org/torproject.org obfs4proxy main" | sudo tee -a /etc/apt/sources.list.d/tor.list
        echo ""
    else
        echo "deb tor+http://deb.torproject.org/torproject.org stable main" | sudo tee /etc/apt/sources.list.d/tor.list
        echo "deb tor+http://deb.torproject.org/torproject.org obfs4proxy main" | sudo tee -a /etc/apt/sources.list.d/tor.list
	    echo ""
    fi
}

function gpgkey {
	gpg --keyserver keys.gnupg.net --recv 886DDD89
	gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | sudo apt-key add -
	echo ""
}

function libevent {
        read -p "libevent-2.0-5 nasb nist, mikhay nsb beshe ya na  [Y/n]: " qa #TODO
        if [ "$qa" == "y" ] || [ "$qa" == "Y" ] || [ "$qa" == "" ]; then
            wget "http://ftp.de.debian.org/debian/pool/main/libe/libevent/libevent-2.0-5_2.0.21-stable-3_amd64.deb" -P ~/Traktor/*/
            sudo dpkg -i ~/Traktor/*/libevent-2.0-5_2.0.21-stable-3_amd64.deb
        else
            echo "no libevent | Abort." #TODO
        fi
}

function libssl {
        read -p "libsll1.1 nasb nist, mikhay nsb beshe ya na  [Y/n]: " qa #TODO
        if [ "$qa" == "y" ] || [ "$qa" == "Y" ] || [ "$qa" == "" ]; then
            wget "http://ftp.de.debian.org/debian/pool/main/o/openssl/libssl1.1_1.1.0f-5_amd64.deb" -P ~/Traktor/*/
            sudo dpkg -i ~/Traktor/*/libssl1.1_1.1.0f-5_amd64.deb
        else
            echo "No libssl | Abort." #TODO
        fi
}

function updatemain {
	sudo apt-get update
	echo ""
	sudo apt-get install -y \
		tor \
		obfs4proxy
	echo ""
}

function panel {
    #if [ ! -d "$HOME/.Traktor/Traktor_Panel" ]; then
    mv Traktor_Panel $HOME/.Traktor/
    chmod +x $HOME/.Traktor/Traktor_Panel/traktor_panel.py
    echo "[Desktop Entry]
Version=1.0
Name=Traktor Panel
Name[fa]=تراکتور پنل
GenericName=Traktor Panel
GenericName[fa]=تراکتور پنل
Comment=Traktor Panel
Comment[fa]=تراکتور پنل
Exec=$HOME/.Traktor/Traktor_Panel/traktor_panel.py
Terminal=false
Type=Application
Categories=Network;Application;
Icon=$HOME/.Traktor/Traktor_Panel/icons/Traktor.png
Keywords=Tor;Browser;Proxy;VPN;Internet;Web" | sudo tee /usr/share/applications/traktor-panel.desktop
    echo ""
    #fi
    #mv Traktor_Panel $HOME/.Traktor/Traktor_Panel
    #mv traktor_panel/icons $HOME/.Traktor/Traktor_Panel
    #chmod +x $HOME/.Traktor/Traktor_Panel/traktor_panel.py
}
# =========/Functions/=========


# =========\Call Functions\=========

sudo uname -a &> /dev/null

echo -e "\n\n\n[ #install ] Install Log: [$(date)]\n" | tee -a $HOME/.Traktor/Traktor_Log/install.log > /dev/null # echo date and time

echo -ne "Installing packages...  "
packin >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Backing up the old torrc to '/etc/tor/torrc.traktor-backup'...   "
torrcbackup >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Writing bridges...   "
bridgewrite >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Fixing apparmor...   "
fixappar >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Backing up privoxy...   "
privoxyback >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Config for privoxy...   "
privoxyconf >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}stop Network Manager from adding dns-servers to /etc/resolv.conf...   "
stopnet >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Set ip and port...   "
setip >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne


# Fix Owner
#chkdebtor=`ls -lha /var/log/tor/log | awk {'print $3'}`
#chkadm=`ls -lha /var/log/tor/log | awk {'print $4'}`
#if [[ "$chkdebtor" != "debian-tor" ]] || [[ "$chkadm" != "adm" ]]; then
#    echo -ne "${NC}fix owner /var/log/tor/log...   "
#    chkowner  >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
#    loading; dne
#fi


echo -ne "${NC}Tor is trying to establish a connection. This may take long for some minutes. Please wait...   "
boot >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Adding  tor repos...   "
addrepo >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne
echo -ne "${NC}Fetching Tor signing key and adding it to the keyring...   "
gpgkey >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne


# Check Package
libsl=`apt list --installed 2> /dev/null | grep libssl1.1`
if [[ "$?" == "0" ]]; then
    libsl_status=true
else
    libsl_status=false
fi

libevnt=`apt list --installed 2> /dev/null | grep libevent-2.0-5`
if [[ "$?" == "0" ]]; then
    libevnt_status=true
else
    libevnt_status=false
fi


if [[ "$libsl_status" == "false" ]] || [[ "$libevnt_status" == "false" ]]; then
    echo -ne "\n${NC}Adding libssl or libevent...   "
    apt list --installed 2> /dev/null | grep libssl >> $HOME/.Traktor/Traktor_Log/install.log
    echo "" | tee -a $HOME/.Traktor/Traktor_Log/install.log
    apt list --installed 2> /dev/null | grep libevent >> $HOME/.Traktor/Traktor_Log/install.log
    if [[ "$libsl_status" == "false" ]]; then
        libssl
    fi
    if [[ "$libevnt_status" == "false" ]]; then
        libevent
    fi
    echo ""
fi

echo -ne "${NC}Updating tor from main repo...   "
updatemain >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
loading; dne


# Panel
if [ ! -d "$HOME/.Traktor/Traktor_Panel" ]; then
    echo -ne "${NC}Create Tray Icon...   "
    panel >> $HOME/.Traktor/Traktor_Log/install.log 2>&1 &
    loading; dne
fi


# =========/Call Functions/=========

echo -e "\n\nCongratulations!!! Your computer is using Tor. may run torbrowser-launcher now."
