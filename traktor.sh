#!/bin/bash


##########################################
#               Traktor V2               #
# https://gitlab.com/TraktorPlus/Traktor #
##########################################


clear


# =========\Create File and Folder\=========

if [ "$1" == "help" ];then
    clear   
    less help
    exit
fi

if [ ! -d "$HOME/.Traktor/" ]; then
    mkdir "$HOME/.Traktor"
fi
if [ ! -d "$HOME/.Traktor/Traktor_Log/" ]; then
    mkdir "$HOME/.Traktor/Traktor_Log"
fi
if [ ! -e "$HOME/.Traktor/Traktor_Log/traktor.log" ]; then
    echo -e "Traktor Log: [$(date)]" | tee $HOME/.Traktor/Traktor_Log/traktor.log > /dev/null
fi
if [ ! -e "$HOME/.Traktor/Traktor_Log/traktor_status.log" ]; then
    echo -e "Traktor Status Log: [$(date)]" | tee $HOME/.Traktor/Traktor_Log/traktor_status.log > /dev/null
fi

# =========/Create File and Folder/=========


# =========\Functions\=========

function restart {
sudo systemctl restart tor.service
echo "Done."
echo -e "\n[$(date)] tor restarted" | tee -a ~/.Traktor/Traktor_Log/traktor_status.log > /dev/null
exit 0
}

function proxyOff {
gsettings set org.gnome.system.proxy mode 'none'
echo "Done."
echo -e "\n[$(date)] gnome system proxy : none" | tee -a ~/.Traktor/Traktor_Log/traktor_status.log > /dev/null
exit 0
}

function proxyOn {
gsettings set org.gnome.system.proxy mode 'manual'
echo "Done."
echo -e "[$(date)] gnome system proxy : manual" | tee -a ~/.Traktor/Traktor_Log/traktor_status.log > /dev/null
exit 0
}

function help { #need more commits
echo -e "OPTIONS        \n\nuninstall           Uninstalls the supported distro \nrestart                       Restarts the tor.service\nproxy-off             Disables the system wide proxy\nproxy-on\
                Enables the system wide proxy\n\nhttps://gitlab.com/TraktorPlus/Traktor" | less
exit 0
}

function uninstall {

if zypper search i+ &> /dev/null ; then
	 if [ ! -f ./uninstall_opensuse.sh ]; then
	 	 wget -O ./uninstall_opensuse.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/uninstall_opensuse.sh' || curl -O  https://gitlab.com/TraktorPlus/Traktor/raw/master/uninstall_opensuse.sh
       	fi
        sudo chmod +x ./uninstall_opensuse.sh
        ./uninstall_opensuse.sh   

elif apt list --installed &> /dev/null ;then
  	if [ ! -f ./uninstall_debian.sh ]; then
   		 wget -O ./uninstall_debian.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/uninstall_debian.sh' || curl -O  https://gitlab.com/TraktorPlus/Traktor/raw/master/uninstall_debian.sh
  	fi
  	sudo chmod +x ./uninstall_debian.sh
  	./traktor_debian.sh
else
    echo "Your distro is neither  debianbase nor susebase So, The script is not going to work in your distro."
fi
echo "traktor has been removed"
echo -e "\n [$(date)] traktor has been removed" || tee -a ~/.Traktor/Traktor_Log/traktor.log > /dev/null
exit 0
}

function none {
echo -e 'Switch not defined.\nPlease read the help "./traktor.sh help"'
exit 1
}

# =========/Functions/=========


# =========\Switchs\=========

#searchs or args to call the right  function
case "$1" in
	"") 	;;
	"help")	        help	   ;;
	"restart")      restart	   ;;
	"proxy-on")	    proxyOn    ;;
	"proxy-off")	proxyOff   ;;
	"uninstall")    uninstall  ;;
	*)	        	none       ;; 
esac

# =========/Switchs/=========


# =========\Main\=========

#Checking if the distro is debianbase / archbase / redhatbase/ openSUSEbae and running the correct script
if pacman -Q &> /dev/null ;then # Check Arch
    if [ ! -f ./traktor_arch.sh ]; then
        wget -O ./traktor_arch.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_arch.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_arch.sh
    fi
    sudo chmod +x ./traktor_arch.sh
    ./traktor_arch.sh # Run Traktor Arch
elif apt list --installed &> /dev/null ;then # Check Debian
    if [ ! -f ./traktor_debian.sh ]; then
        wget -O ./traktor_debian.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_debian.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_debian.sh
    fi
    sudo chmod +x ./traktor_debian.sh 
    ./traktor_debian.sh # Run Traktor Debian
elif dnf list &> /dev/null ;then
    if [ ! -f ./traktor_fedora.sh ]; then # Check RedHat
        wget -O ./traktor_fedora.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_fedora.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_fedora.sh
    fi
    sudo chmod +x ./traktor_fedora.sh
    ./traktor_fedora.sh # Run Traktor Fedora
elif zypper search i+ &> /dev/null ;then
    if [ ! -f ./traktor_opensuse.sh ]; then # Check OpenSUSE
        wget -O ./traktor_opensuse.sh 'https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_opensuse.sh' || curl -O https://gitlab.com/TraktorPlus/Traktor/raw/master/traktor_opensuse.sh
    fi
    sudo chmod +x ./traktor_opensuse.sh
    ./traktor_opensuse.sh # Run Traktor OpenSUSE
else
    echo "Your distro is neither archbase nor debianbase nor redhatbase nor susebase So, The script is not going to work in your distro."
fi


echo -e "\n[$(date)] traktor installed " | tee -a ~/.Traktor/Traktor_Log/traktor.log > /dev/null
