# Traktor V2 Beta
Traktor will autamically install Tor, privoxy, dnscrypt-proxy and Tor Browser Launcher in either a Debian based distro like Ubuntu, an Arch based distro, Fedora based distro or an OpenSUSE based distro and configures them as well.

To do this, just run 'traktor.sh' file in a supported shell like bash and watch for prompts it asks you.

## Note
Do NOT expect anonymity using this method. Privoxy is an http proxy and can leak data. If you need anonymity or strong privacy, manually run torbrowser-launcher after installing traktor and use it.

## Manual Pages
[راهنمای اسکریپت تراکتور به زبان فارسی](https://soshaw.net/traktor/) {lang=FA}

## Install
### Ubuntu and other Distro
    wget https://gitlab.com/TraktorPlus/Traktor/repository/archive.zip?ref=Traktor-V2-Beta -O traktor-V2-beta.zip
    unzip traktor-V2-beta.zip -d  ~/Traktor-V2-Beta && cd Traktor-V2-Beta/*
    ./traktor.sh
### ArchLinux
    yaourt -S traktor

## Changes
[See Changes](https://gitlab.com/TraktorPlus/Traktor/blob/master/CHANGELOG)
