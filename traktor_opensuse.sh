#!/bin/bash
clear
echo -e "Traktor v2\nTor will be automatically installed and configured…\n\n"

GRE='\033[0;32m'
NC='\033[0m'

function loading {


	spin[0]="-"
	spin[1]="\\"
	spin[2]="|"
	spin[3]="/"
	PID=$!
	echo -n "${spin[0]}"
	while [ -d /proc/$PID ]
	do
	        for i in "${spin[@]}"
                do
	                echo -ne "\b$i"
	                sleep 0.2
		done
	done
	echo -e "\b${GRE}Done."
}

#Tumbleweed will add as soon as possible
function packin {
	if grep -Fxq "VERSION=\"42.3\"" /etc/os-release
	then
		
		sudo zypper addrepo http://download.opensuse.org/repositories/home:hayyan71/openSUSE_Leap_42.3/home:hayyan71.repo #add obfs4proxy
		sudo zypper addrepo http://download.opensuse.org/repositories/server:dns/openSUSE_42.3/server:dns.repo #add dnscrypt-proxy

	elif grep -Fxq "VERSION=\"42.2\"" /etc/os-release
	then
	
		sudo zypper addrepo http://download.opensuse.org/repositories/home:hayyan71/openSUSE_Leap_42.2/home:hayyan71.repo #add obfs4proxy
		sudo zypper addrepo http://download.opensuse.org/repositories/server:dns/openSUSE_42.2/server:dns.repo #add dnscrypt-proxy

	else
		echo "\n\n tratktor doesn't support your current version yet"
		exit 1
	fi
	
	sudo zypper --no-gpg-checks ref
	#Install Packages
	sudo zypper in -l -y obfs4proxy dnscrypt-proxy privoxy


}

function torrcbackup {

	if [ -f "/etc/tor/torrc" ]; then
		sudo cp /etc/tor/torrc /etc/tor/torrc.traktor-backup
	fi
}

function bridgewrite {

	sudo wget -q https://gitlab.com/TraktorPlus/Traktor/raw/config/torrc -O /etc/tor/torrc > /dev/null
	sudo sed -i '1 i\SOCKSPolicy accept 127.0.0.1:9050' /etc/tor/torrc
	sudo sed -i -- 's/Log notice file \/var\/log\/tor\/log/Log notice file \/var\/log\/tor\/tor.log/g' /etc/tor/torrc
}


function privoxyback {
	
	if [ -f "/etc/privoxy/config" ]; then
	        sudo cp /etc/privoxy/config /etc/privoxy/config.traktor-backup
	fi
}

function privoxyconf {
	sudo perl -i -pe 's/^listen-address/#$&/' /etc/privoxy/config
	echo 'logdir /var/log/privoxy
	listen-address  0.0.0.0:8118
	forward-socks5t             /     127.0.0.1:9050 .
	forward         192.168.*.*/     .
	forward            10.*.*.*/     .
	forward           127.*.*.*/     .
	forward           localhost/     .' | sudo tee /etc/privoxy/config > /dev/null
	sudo systemctl enable privoxy
	sudo systemctl restart privoxy.service
}

function setip {
	gsettings set org.gnome.system.proxy mode 'manual'
	gsettings set org.gnome.system.proxy.http host 127.0.0.1
	gsettings set org.gnome.system.proxy.http port 8118
	gsettings set org.gnome.system.proxy.socks host 127.0.0.1
	gsettings set org.gnome.system.proxy.socks port 9050
	gsettings set org.gnome.system.proxy ignore-hosts "['localhost', '127.0.0.0/8', '::1', '192.168.0.0/16', '192.168.8.1', '10.0.0.0/8', '172.16.0.0/12', '0.0.0.0/8', '10.0.0.0/8', '100.64.0.0/10', '127.0.0.0/8', '169.254.0.0/16', '172.16.0.0/12', '192.0.0.0/24', '192.0.2.0/24', '192.168.0.0/16', '192.88.99.0/24', '198.18.0.0/15', '198.51.100.0/24', '203.0.113.0/24', '224.0.0.0/3']"
}

sudo uname -a &> /dev/null
mkdir .log
echo -n "Installing packages...  "
packin >>~/.log/install.log 2>&1 &
loading
echo -ne "${NC}Backing up the old torrc to '/etc/tor/torrc.traktor-backup'...   "
torrcbackup >>~/.log/install.log 2>&1 &
loading
echo -ne "${NC}Writing bridges...   "
bridgewrite >> ~/.log/install.log 2>&1 &
loading
echo -ne "${NC}Backing up privoxy...   "
privoxyback >>~/.log/install.log 2>&1 &
loading
echo -ne "${NC}Config for privoxy...   "
privoxyconf >>~/.log/install.log 2>&1 &
loading
echo -ne "${NC}Set ip and port...   "
setip >>.~/.log/install.log 2>&1 &
loading


echo -e "\nInstall Finished successfully…\n"
# Wait for tor to establish connection
echo -e "${NC}Tor is trying to establish a connection. This may take long for some minutes. Please wait${GRE}" | sudo tee /var/log/tor/tor.log
bootstraped='n'
sudo systemctl enable tor.service 
sudo systemctl restart tor.service
while [ $bootstraped == 'n' ]; do
	if sudo cat /var/log/tor/tor.log | grep "Bootstrapped 100%: Done";then
		bootstraped='y'
	else
		sleep 1
 	fi 
done 

echo -e "\n\nCongratulations!!! Your computer is using Tor. If you are using KDE , set IP and PORT manually.${NC}"
